package fr.cea.kemanager.dpos.encrypt.test;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.bcel.classfile.Constant;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.cea.kemanager.dpos.encrypt.helper.Constants;
import fr.cea.kemanager.dpos.encrypt.helper.Tools;

import static org.junit.Assert.assertEquals;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;

import javax.crypto.SecretKey;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test") // load application-test.properties
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MethodTest {
	//@Test
    public void testCompareHash() {

        System.out.println("Exec test compare hash256");

		String encryptedDsaFileOrigine ="vault:v1:1z9sjZfWxOjZ+6rRPlRNuIkzvlM23Sr9Zb+6DB+obXP3pcxe8RWgDKY86gIy+cRZrVD6HK8nszS3L8TCSJ+";
		String encryptedCtiFileOrigine ="vault:v1:RFDOPyhzYyJHwczkXKHeRRDsbxhgfUq75xa6a386cg10++ezoSm03gIhODf+gXPVn4VH7kpXnW7DVRfDISFBA";
				
		String encryptedDsaFileAfter ="vault:v1:1z9sjZfWxOjZ+6rRPlRNuIkzvlM23Sr9Zb+6DB+obXP3pcxe8RWgDKY86gIy+cRZrVD6HK8nszS3L8TCSJ+";
		String encryptedCtiFileAfter ="vault:v1:RFDOPyhzYyJHwczkXKHeRRDsbxhgfUq75xa6a386cg10++ezoSm03gIhODf+gXPVn4VH7kpXnW7DVRfDISFBA";
		
		String expectedHash = "1f26bef4265a39a4dada134faeeb296c9624d828a22463d05b2bfe17d7e89ba9";
		String computedHashAfter = "";
		String computedHashOrigine = "";
	
		
		
		//computedHashOrigine = Tools.checksumSHA256(encryptedCtiFileOrigine+""+encryptedDsaFileOrigine);
		//computedHashAfter = Tools.checksumSHA256(encryptedCtiFileAfter+""+encryptedDsaFileAfter);
		System.out.println("computedHashOrigine = " + computedHashOrigine);
		System.out.println("computedHashAfter = " + computedHashAfter);
		
		assertEquals(expectedHash, computedHashAfter);
    }

    //@Test
    public void testHmacFileContent() {
		File fContent = new File("/home/nguyen/Documents/SAMPLE_DPO_metadata/test_File.txt");
		String strContent = "2019-01-07T10:02:27.000+0100\n" + 
				"===============================================================================================================================================\n" + 
				"ERROR File : /home/nguyen/Documents/SAMPLE_DPO_metadata/create-BundleManager/ISP-pilot/File_test_20190110/ssh/2019-01-07T10:02:27.000+0100.fgt\n" + 
				"===============================================================================================================================================\n";
		
		String hmacStrContent = Tools.encodeHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, strContent.getBytes()); 
		String hmacFContent = Tools.encodeFileHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, fContent);
		System.out.println("hmacStrContent = " + hmacStrContent);
		System.out.println("hmacFContent   = " + hmacFContent);
		
		assertEquals(hmacStrContent, hmacFContent);
	}
	
   // @Test
    public void testCompareFileContent() {
    	System.out.println("testCompareFileContent");
    	File fContentHadoop = new File("/home/nguyen/debug/error/1547500560806-15c8bfad-363c-4b9a-a7cb-8bc4f44ed2a5.payload");
		File f2ContentTmp =new File("/home/nguyen/debug/error/1547500560806-15c8bfad-363c-4b9a-a7cb-8bc4f44ed2a56262075570303073953_create.payload");
		
		String hmacfContentHadoop = Tools.encodeFileHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, fContentHadoop); 
		String hmacFf2ContentTmp = Tools.encodeFileHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, f2ContentTmp);
		System.out.println("hmacfContentHadoop = " + hmacfContentHadoop);
		System.out.println("hmacFf2ContentTmp   = " + hmacFf2ContentTmp);
		
		assertEquals(hmacfContentHadoop, hmacFf2ContentTmp);
	}
	


    @Test
	public void testDecodeBase64File() {
		System.out.println("Test DecodeBase64File now ");
		//String encryptedDsaFileAfter ="/home/nguyen/Desktop/dpo_id_decodebase64.txt";
		//String encryptedDsaFileAfter ="/home/nguyen/Desktop/dpo_id_decodebase64_2.txt";
		//File encode64 = new File(encryptedDsaFileAfter); 
		try {
			//byte[] content = FileUtils.readFileToByteArray(encode64);
			//String contentString = FileUtils.readFileToString(encode64);
			String contentString = "dmF1bHQ6djE6eCtPSTl6NWs1TUdTLzNYbnM5TldlNUJlK1pwZ0hJWWFFUFZsTTJTczZ5MkxwQnZUY0JSVFQyZFhYc2I0"
					+ "NktjT3djSnJBcXByNWdPVFBHM0o1VVpKbm1KWEVjNjdUTmRWUnd6ZEdabU5HZXhaL2VST1YzalJLTTJ1SWVhOE14VlBadzAxZml3VlF0blQ1MG"
					+ "RIWVFwb2U0Ly9vUE54QmY0Q2ZmNmJJZUlBL3c4bktJcWYyaVZzMUMxK2NwZmVvaCt3RWkwRjN6MWpHbkIrdUVEUzlERHI0Vm52Y3BROTI5SlEvb"
					+ "G9HWTI1NUh2TlVBMVpQK1dpSnRSeG95L0o2dTB3UHYySF";
			
			byte[] b;	
			String contentDecodeBase64 = "";
		    if (contentString != null) {
		        BASE64Decoder decoder = new BASE64Decoder();
		        b = decoder.decodeBuffer(contentString);
		        contentDecodeBase64 = new String(b, StandardCharsets.UTF_8);		        
		    }
		    System.out.println("using BASE64Decoder = " + contentDecodeBase64);
			
		    //Using this Java.util.base64 raises error
			//String contentDecodeBase64 = new String( 
				//	Base64.getDecoder().decode(contentString.getBytes()), 				 
				//	StandardCharsets.UTF_8);
			//java.lang.IllegalArgumentException: Input byte array has incorrect ending byte at 96840
			//String contentDecodeBase64 = new String( 
					//Base64.getDecoder().decode(content), 				 
//					StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}
	}
