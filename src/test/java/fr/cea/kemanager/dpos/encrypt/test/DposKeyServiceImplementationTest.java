/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package fr.cea.kemanager.dpos.encrypt.test;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import fr.cea.kemanager.dpos.encrypt.ApplicationDeployer;
import fr.cea.kemanager.dpos.encrypt.restapi.DataApiController;

/**
 * @author MIMANE
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=ApplicationDeployer.class)
@WebMvcTest(DataApiController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test") // load application-test.properties
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DposKeyServiceImplementationTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;

	@Test
    public void test01get() throws Exception {
        
        String param = "test";
        String expectedOutput = "aName";
        
        System.out.println(">>>>>>>>>>>"+restUser);
//        this.mockMvc.perform(
//                get("/v1/template/" + param + "/")
//                    .with(httpBasic(restUser, restPassword)) // basic auth
//                    .accept(MediaType.APPLICATION_JSON)
//                    .contentType(MediaType.APPLICATION_JSON)
//                 )
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(content().string(containsString(expectedOutput))) //check if the returned output (json) contains 'expectedOutput
//                ;
    }
	
	@Test
    public void test02get() throws Exception {
        
        String param = "test";
        //expected output structure = "{\"name\":\"aName\",\"id\":\"anIdValue\",\"path\":\"aPath\",\"version\":\"1\"}";
        
//        this.mockMvc.perform(
//                get("/v1/template/" + param + "/")
//                    .with(httpBasic(restUser, restPassword)) // basic auth
//                    .header("X-myheader-test1", "TEST1")
//                    .header("X-myheader-test2", "TEST2")
//                    .accept(MediaType.APPLICATION_JSON)
//                    .contentType(MediaType.APPLICATION_JSON)
//                 )
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.name", is("aName")))
//                ;
        
    }
}
