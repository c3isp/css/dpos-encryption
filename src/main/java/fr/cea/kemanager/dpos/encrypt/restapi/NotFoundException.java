package fr.cea.kemanager.dpos.encrypt.restapi;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-16T10:31:18.377Z")

public class NotFoundException extends ApiException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1013089916822836298L;
	private int code;
    public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public NotFoundException (int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
