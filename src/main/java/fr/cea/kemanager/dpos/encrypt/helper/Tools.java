package fr.cea.kemanager.dpos.encrypt.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fr.cea.kemanager.dpos.encrypt.restapi.DataApi;
import fr.cea.kemanager.dpos.encrypt.vault.*;
import fr.cea.kemanager.lib.DataObject;
import fr.cea.kemanager.vault.common.VaultStart;

public class Tools {
	static Logger log = LoggerFactory.getLogger(Tools.class);


	public static ResponseEntity<DataObject> decryptDataWithAES(
			@NotNull DataObject 	fileData, 
			@NotNull String 		requestId, 
			@NotNull String 		dsaId, 
			@NotNull String 		TokenRoot) throws IOException 
	{
		StopWatch stopwatch = new StopWatch();						
		stopwatch.start();			
		AESEncryption aESEncrypt = new AESEncryption(new VaultStart(TokenRoot).GetTransit());
		
		String HmacBinary = Tools.encodeHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, fileData.getDataContent().getBytes());
		log.info("[DPOS-DECRYPTION] BEFORE DECRYPTION HMAC =<"+HmacBinary+">");
		String messageBase64 = aESEncrypt.DecryptAES(fileData.getDataContent(), dsaId);
		
		long timeElapsed = stopwatch.getTime();
		log.info("DECRYPT content from Vault, Execution time in milliseconds: " + timeElapsed);		
		
		/**
		 * Using this code below for disable
		 * */
//		String messageBase64 = fileData.getDataContent();		
//		//Temporary disable decrypt & encrypt from Vault 
//		if(messageBase64.contains("vault:"))
//		{
//			messageBase64 = aESEncrypt.DecryptAES(fileData.getDataContent(), dsaId);		
//		}
		log.info("[DPOS-DECRYPTION] Decryption request ok ! " + requestId + " Preparing to back descrypt result now !"); 					
		DataObject dataObject = new DataObject()
				.dataContent(messageBase64)
				.dataName(dsaId + "_decrypted")
				.urlToBack("http_url_to_back");
		
		HmacBinary = Tools.encodeHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, messageBase64.getBytes());
		log.info("[DPOS-DECRYPTION] AFTER DECRYPTION HMAC =<"+HmacBinary+">");		
		dataObject.setChecksum(HmacBinary);

		log.info("[DPOS-DECRYPTION] Successfully request " + requestId + " DSAID = " + dsaId);
		stopwatch.stop();
		return new ResponseEntity<DataObject>(dataObject, HttpStatus.OK);
	}
	
	public static ResponseEntity<DataObject> decryptDataWithKreyvium(
			@NotNull String fieldToDecrypt, 
			@NotNull String offsetValues,
			@NotNull String dsaId, 
			@NotNull String dataContent, 
			@NotNull String TokenRoot) throws IOException 
	{
			ArrayList<String> txtEncryptedValue 	= __extractValueFromFieldText(dataContent, fieldToDecrypt);
			Map<String, String> decryptedTextValue = __kreyviumDecryption(txtEncryptedValue, dsaId, offsetValues, dataContent, TokenRoot);
			if(decryptedTextValue.size() != 2) throw new IOException("Error during decrytion specific field with kreyvium !");
			String decryptedDataContent = decryptedTextValue.get(Constants.DefaultParam.ENCRYPTED_FIELD_PARAM);						

			DataObject dataObject = new DataObject()
					.dataContent(decryptedDataContent)
					.dataName(dsaId + "_decrypted")
					.urlToBack("http_url_to_back");
			dataObject.setChecksum(
					Objects.hash(dataObject.getDataContent())+"");
//			if(fileData.getOtherProperties() != null && fileData.getOtherProperties().size() > 0)
//			{
//				dataObject.getOtherProperties().put(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM, 
//						fileData.getOtherProperties().get(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM));
//			}
			return new ResponseEntity<DataObject>(dataObject, HttpStatus.OK);
	}


	public static ResponseEntity<DataObject> applyingEncryptionBundle(
			@NotNull DataObject 	fileData, 
			@NotNull String 		requestId, 
			@NotNull String 		dsaId, 
			@NotNull String 		TokenRoot) throws IOException 
	{
		StopWatch stopwatch = new StopWatch();						
		stopwatch.start();			
		
		AESEncryption aESEncrypt = new AESEncryption(new VaultStart(TokenRoot).GetTransit());
		String HmacBinary = Tools.encodeHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, fileData.getDataContent().getBytes());
		log.info("[DPOS-ENCRYPTION] BEFORE ENCRYPTING HMAC =<"+HmacBinary+">");
		//String messageBase64 = fileData.getDataContent();
		String messageBase64 = aESEncrypt.EncryptAES(fileData.getDataContent(),dsaId);
				
		long timeElapsed = stopwatch.getTime();
		log.info("Encrypt content from Vault, Execution time in milliseconds: " + timeElapsed);
		
		File encryptedMessageBase64 = Tools.createTempFile(UUID.randomUUID().toString(), "data", messageBase64);
		String HmacShaCtiFile = Tools.encodeFileHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, encryptedMessageBase64);
		
		DataObject dataObject = new DataObject()
				.dataContent(messageBase64)
				.dataName(dsaId + "_encrypted")
				.urlToBack("http_url_to_back");				
		dataObject.setChecksum(HmacShaCtiFile);
		
		log.info("[DPOS-ENCRYPTION] Successfully encryption request " + requestId + " DSAID = " + dsaId 
				+ " with sending content length = " + dataObject.getDataContent().length() 
				+ " \n AFTER ENCRYPTING HMAC <" + HmacShaCtiFile + ">" 
				+ " File : " + encryptedMessageBase64.getAbsolutePath()	);
		
		log.info("Encrypt content, Execution time in milliseconds: " + (stopwatch.getTime() - timeElapsed));
		timeElapsed = stopwatch.getTime();
		stopwatch.stop();
		return new ResponseEntity<DataObject>(dataObject, HttpStatus.OK);
	}

	public static ResponseEntity<DataObject> applyingTranscipheringProcess(
			@NotNull DataObject 	fileData, 
			@NotNull String 		dsaId, 
			@NotNull String 		fieldToEncrypt, 
			@NotNull String 		TokenRoot) throws IOException 
	{		
		ArrayList<String> txtValueToEncrypt 	= __extractValueFromFieldText(fileData.getDataContent(), fieldToEncrypt);
		Map<String, String> encryptedTextValue  = __kreyviumEncryption(txtValueToEncrypt, dsaId, fileData.getDataContent(), TokenRoot);
		if(encryptedTextValue.size() != 2) throw new IOException("Error during encrytion specific field with kreyvium !");
		fileData.setDataContent(encryptedTextValue.get(Constants.DefaultParam.CONTENT_FILE_PARAM));
		fileData.putOtherPropertiesItem(
				Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM, 
				encryptedTextValue.get(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM));
		
		return new ResponseEntity<DataObject>(fileData, HttpStatus.OK);
	}

	private static ArrayList<String> __extractValueFromFieldText(
			@NotNull String dataContent, 
			@NotNull String fieldToEncrypt) 
	{
		String[] actualContent = dataContent.split("\\|");
		ArrayList<String> res = new ArrayList<String>();
		for (String s : actualContent) {
			String[] parts = s.split(" ");
			for (int i=0; i<parts.length;i++) {
				if (parts[i].split("=")[0].compareTo(fieldToEncrypt)==0) {
					res.add(parts[i].split("=")[1]);
				}
			}
		}
		return res;
	}

	private static String __replaceEncryptedValueFromFieldText(
			@NotNull String fileDataString, 
			@NotNull String txtValueToEncrypt,
			@NotNull String strNewValue) 
	{
		String result = fileDataString;
		result = result.replaceFirst(txtValueToEncrypt, strNewValue);
		return result;
	}

	public static Map<String, String> __kreyviumEncryption(			
			ArrayList<String> 	txtValueToEncrypt, 
			@NotNull String 	dsAId, 
			@NotNull String 	fullText, 
			@NotNull String 	tokenRoot) 
	{
		KVEncryption kVEncrypt = new KVEncryption(new KVKeyManager(new VaultStart(tokenRoot).GetTemplate()));
		String _encryptedIPparts;
		String curr = fullText;
		for (String s : txtValueToEncrypt) {
			_encryptedIPparts = kVEncrypt.EncryptIPKV(s, dsAId);
			curr = __replaceEncryptedValueFromFieldText(curr, s, _encryptedIPparts);
		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Constants.DefaultParam.CONTENT_FILE_PARAM, curr);
		map.put(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM, "0");
		return map;
	}


	private static Map<String, String> __kreyviumDecryption(			
			ArrayList<String> txtValueToDecrypt, 
			@NotNull String dsaId,
			@NotNull String offsetValues, 
			@NotNull String fullText, 
			@NotNull String tokenRoot) 
	{		
		KVEncryption kVEncrypt = new KVEncryption(new KVKeyManager(new VaultStart(tokenRoot).GetTemplate()));
		String _decryptedIPparts;
		String curr = fullText;
		for (String s : txtValueToDecrypt) {
			_decryptedIPparts = kVEncrypt.DecryptIPKV(s, dsaId);
			curr = __replaceEncryptedValueFromFieldText(curr, s, _decryptedIPparts);
		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Constants.DefaultParam.CONTENT_FILE_PARAM, curr);
		map.put(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM, "0");
		return map;
	}
	
	public static String encodeHmacSHA(byte[] data) {
		return encodeHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, data);
	}
	
	public static String encodeHmacSHA(String key, byte[] data) {
	    try {

	        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
	        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
	        sha256_HMAC.init(secret_key);

	        return new String(Hex.encodeHex(sha256_HMAC.doFinal(data)));	    
	    } catch (InvalidKeyException e) {
	        e.printStackTrace();
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }
	    return null;
	}

	public static String encodeFileHmacSHA(File initialFile)
	{
		return encodeFileHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, initialFile);
	}
	
	public static String encodeFileHmacSHA(String key, File initialFile)
	{
		try {
			return encodeFileHmacSHA(key, new FileInputStream(initialFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static String encodeFileHmacSHA(InputStream in)
	{
		try {
			return encodeFileHmacSHA(Constants.SECRET_KEY_MSG_AUTHENTICATION, in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	public static String encodeFileHmacSHA(String key, InputStream in) throws java.io.IOException
	{
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			
			byte[] ibuf = new byte[1024];
			int len;
			while ((len = in.read(ibuf)) != -1) {
				sha256_HMAC.update(ibuf, 0, len);
			}
			return new String(Hex.encodeHex(sha256_HMAC.doFinal()));
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static File createTempFile(String fileName, String suffix, String fileObjectContent)
			throws IOException, FileNotFoundException {
		// Since Java 1.7 Files and Path API simplify operations on files
		Path path = Files.createTempFile(fileName, suffix);
        File file = path.toFile();
        // writing sample data
        Files.write(path, fileObjectContent.getBytes(StandardCharsets.UTF_8));
        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        file.deleteOnExit();
        return file;
	}    
	
	public static File createTempFile(String fileName, String suffix)
			throws IOException, FileNotFoundException {
		// Since Java 1.7 Files and Path API simplify operations on files
		Path path = Files.createTempFile(fileName, suffix);
        File file = path.toFile();
        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        file.deleteOnExit();
        return file;
	}    
	
	public static File createTempFile(String fileName, String suffix, 
			byte[] fileObjectContent) throws IOException {
		return createTempFile(fileName, suffix, 
				new String(fileObjectContent, StandardCharsets.UTF_8));
	}
	
	public static byte[] ZipContent(List<File> srcFiles, String fileName) throws FileNotFoundException, IOException {
		File file = __zipFile(srcFiles, fileName);
		Path path = Paths.get(file.getAbsolutePath());
		byte[] data = null;
		data = Files.readAllBytes(path);
		return data;
	}
	
	private static File __zipFile(List<File> srcFiles, String fileName) throws FileNotFoundException, IOException {
		File file = Tools.createTempFile(fileName, ".zip");
		FileOutputStream   fos = new FileOutputStream(file);
		ZipOutputStream zos = new ZipOutputStream(fos);
		byte[] buffer = new byte[128];
		for (File currentFile : srcFiles) {
			if (!currentFile.isDirectory()) {
				ZipEntry entry = new ZipEntry(currentFile.getName());
				FileInputStream fis = new FileInputStream(currentFile);
				zos.putNextEntry(entry);
				int read = 0;
				while ((read = fis.read(buffer)) != -1) {
					zos.write(buffer, 0, read);
				}
				zos.closeEntry();
				fis.close();
			}
		}
		zos.close();
		fos.close();
		return file;
	}
	
}
