/**
 * NOTE: This class is auto generated by the swagger code generator program (1.0.11).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package fr.cea.kemanager.dpos.encrypt.restapi;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.kemanager.lib.DataObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-16T10:31:18.377Z")

@Api(value = "Data", description = "the Data API")
public interface DataApi {

	Logger log = LoggerFactory.getLogger(DataApi.class);

    default Optional<ObjectMapper> getObjectMapper() {
        return Optional.empty();
    }

    default Optional<HttpServletRequest> getRequest() {
        return Optional.empty();
    }

    default Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }

    @ApiOperation(value = "decrypt a bundle data with a required request Id, DSAId, and other security parameters", nickname = "decrypt", notes = "store Payload With Metadata, this is used when someone calls the Create CTI from ISI API", response = DataObject.class, tags={ "data", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Server response", response = DataObject.class),
        @ApiResponse(code = 400, message = "Bad request", response = String.class),
        @ApiResponse(code = 404, message = "Not found", response = String.class) })
    @RequestMapping(value = "/dpos/decrypt/{RequestId}/dsa/{DSAId}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<DataObject> decrypt(@ApiParam(value = "The CTI content to upload." ,required=true )  @Valid @RequestBody DataObject fileData,@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("{  \"urlToBack\" : \"urlToBack\",  \"dataContent\" : \"\",  \"checksum\" : \"checksum\",  \"dataName\" : \"dataName\"}", DataObject.class), HttpStatus.NOT_IMPLEMENTED);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "create a bundle data with a required request Id, DSAId, and other security parameters", nickname = "encrypt", notes = "store Payload With Metadata, this is used when someone calls the Create CTI from ISI API", response = DataObject.class, tags={ "data", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Server response", response = DataObject.class),
        @ApiResponse(code = 400, message = "Bad request", response = String.class),
        @ApiResponse(code = 404, message = "Not found", response = String.class) })
    @RequestMapping(value = "/dpos/encrypt/{RequestId}/dsa/{DSAId}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<DataObject> encrypt(@ApiParam(value = "The CTI content to upload." ,required=true )  @Valid @RequestBody DataObject fileData,@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("{  \"urlToBack\" : \"urlToBack\",  \"dataContent\" : \"\",  \"checksum\" : \"checksum\",  \"dataName\" : \"dataName\"}", DataObject.class), HttpStatus.NOT_IMPLEMENTED);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
