package fr.cea.kemanager.dpos.encrypt.helper;

public class Constants {
	
	public static final String SEPARATOR_PARAM 	= "_TRANS_";
	public static final String SECRET_KEY_MSG_AUTHENTICATION = "78b4c2c6c7808000923e9f23c6dad1434cc53f6da99c2c5f2bcec4d6400198d5";
	
	public class DefaultParam
	{
		public static final String REQUEST_ID_PARAM 	= "requestID";
		public static final String PAYLOAD_FORMAT_PARAM = "payloadFormat";		
		public static final String CONTENT_FILE_PARAM 	= "fileContent";
		public static final String ENCRYPTED_FIELD_PARAM = "encryptedField";
		public static final String OFFSET_KEYSTREAM_PARAM = "offsetTranscrypting";
	}
	
	public class EventHandleParam
	{		
		public static final String METADATA_FILE_PARAM 	= "metadataFile";
		public static final String CTI_FILE_PARAM 		= "ctiFile";
		public static final String DSA_ID_PARAM 		= "dsaId";
		public static final String DPOS_ID_PARAM 		= "dposId";		
	}
	
	public class BundleResponseParam
	{				
		public static final String FILE_NAME_PARAM 		= "fileName";
		public static final String STATUS_RESULT_PARAM 	= "result";	
	}
	
	public class C3ispComponents
	{
		public static final String ISI_NODE 			= "isic3isp";
		public static final String CSS_NODE	 			= "cssc3isp";		
		public static final String ISI_BM_COMPONENT 	= "isibmcom";
		public static final String CSS_DPOS_COMPONENT	= "cssdposcom";
	}
	
	public class BundleManagerSuffixFile
	{
		public static final String METADATA_SUFFIX 	= ".head";
		public static final String CTI_SUFFIX 		= ".payload";
		public static final String DSA_SUFFIX 		= ".dsa";
		public static final String HASH_SUFFIX 		= ".sign";
	}
}
