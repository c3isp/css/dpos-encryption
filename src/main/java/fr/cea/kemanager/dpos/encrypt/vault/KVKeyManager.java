package fr.cea.kemanager.dpos.encrypt.vault;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Collections;

import org.springframework.vault.core.VaultTemplate;

public class KVKeyManager {


	/**
	 * This class encapsulates the operations for Kreyvium key management from Vault.
	 *
	 * @author Quentin Lutz
	 */

	private VaultTemplate m_temKeyValue;

	/**
	 * Create a new {@link KVKeyManager} with a {@link VaultTemplate}.
	 *
	 * @param transit must not be {@literal null}.
	 */
	public KVKeyManager(VaultTemplate template) {
		this.m_temKeyValue = template;
	}

	/**
	 * Create a new KV key.
	 *
	 * @param userID is the ID for the active user.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void CreateKVKey(String userID) {
		int ver = GetVersion(userID);
		String nextVer = Integer.toString(ver+1);
		String keyId = BuildKeyId(ver+1,userID);
		String verId = __BuildVerId(userID);
		m_temKeyValue.write("secret/"+verId, Collections.singletonMap("value",nextVer));
		m_temKeyValue.write("secret/"+keyId,
				Collections.singletonMap("key",new String(GenerateNewKey(),
						StandardCharsets.ISO_8859_1)));
	}

	/**
	 * Return the values of a given KV key.
	 *
	 * @param userID is the ID for the active user.
	 * @param version is the desired version
	 */
	public int[] GetKVKey(String userID, int version) {
		CreateIfNull(userID);
		int ver = GetVersion(userID);
		if (version>ver) {
			return null;
		}
		if (version==0) version = 1; 
		String keyId = BuildKeyId(version,userID);
		byte[] response = ((String) m_temKeyValue.read("secret/"+keyId)
				.getData().get("key"))
				.getBytes(StandardCharsets.ISO_8859_1);
		return byteToInt(response);
	}

	/**
	 * Return the values of the last version
	 * of a given KV key.
	 *
	 * @param userID is the ID for the active user.
	 */
	public int[] GetKVKey(String userID) {
		CreateIfNull(userID);
		int ver = GetVersion(userID);
		return GetKVKey(userID, ver);
	}



	/**
	 * Return the last version number of a
	 * given KV key triplet.
	 *
	 * @param userID is the ID for the active user.
	 */
	public int GetVersion(String userID) {
		boolean exists = (m_temKeyValue.read("secret/"+__BuildVerId(userID))!=null);
		if (exists) {
			return Integer.valueOf((String) m_temKeyValue.read(
					"secret/"+ __BuildVerId(userID)).getData().get("value"));
		} else {
			return 0;
		}
	}

	/**
	 * Return the ID to be used in Vault
	 * for the given parameters to check
	 * key values.
	 *
	 * @param ver is the version number for the desired key.
	 * @param userID is the ID for the active user.
	 */
	public String BuildKeyId(int ver, String userID) {
		return ("v"+Integer.toString(ver)+":KV:"+userID);
	}

	/**
	 * Return the ID to be used in Vault
	 * for the given parameters to check
	 * key versions.
	 *
	 * @param userID is the ID for the active user.
	 */
	private String __BuildVerId(String userID) {
		return "verKV:"+userID;
	}

	/**
	 * Create a new KV key
	 * if none was created previously
	 * for the designated user.
	 * 
	 * @param userID is the ID for the active user.
	 */
	public void CreateIfNull(String userID) {
		boolean exists = (m_temKeyValue.read("secret/"+__BuildVerId(userID))!=null);
		if (!exists) {
			this.CreateKVKey(userID);
		}
	}

	/**
	 * Generate 16 random bytes
	 * (128 bits) to use as a
	 * key for Kreyvium.
	 */
	private byte[] GenerateNewKey() {
		SecureRandom sr = new SecureRandom();
		byte[] key = new byte[16];
		sr.nextBytes(key);
		return key;
	}

	/**
	 * Turn a {@link byte[]} KV key
	 * with length 16 to a {@link int[]}
	 * with length 128 containing the
	 * equivalent bits.
	 * 
	 * @param in is the key in a {@link byte[]} form.
	 */
	private int[] byteToInt(byte[] in) {
		String str;
		int offset;
		int count = 0;
		int[] bitKey = new int[128];
		for (byte b : in) {
			str = Integer.toBinaryString(b+128);
			offset = 8-str.length();
			for (int i = 0; i<offset;i++) {
				bitKey[count++] = 0;
			}
			for (String c : str.split("")) {
				bitKey[count++] = Integer.valueOf(c);
			}
		}
		return bitKey;
	}

}
