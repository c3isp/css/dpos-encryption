package fr.cea.kemanager.dpos.encrypt.vault;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.springframework.vault.core.VaultTransitTemplate;
import org.springframework.vault.support.VaultTransitContext;



/**
 * This class encapsulates the operations for RSA encryption from Vault.
 *
 * @author Quentin Lutz
 */
public class RSAEncryption {

	/*This value is the cap set by Vault for RSA encryption.
	* It can be derived from Lenstra's cryptographic key length equations.
	*/
	private static final int m_intMaxCipherLength = 446;

	
	private VaultTransitTemplate m_temTransit;

	/**
	 * Create a new {@link RSAEncryption} with a {@link VaultTransitTemplate}.
	 *
	 * @param transit must not be {@literal null}.
	 */
	public RSAEncryption(VaultTransitTemplate transit) {
		this.m_temTransit = transit;
	}


	/**
	 * Encrypt data with the RSA key pair of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param plaintext is the data to be encrypted.
	 */
	public byte[] EncryptRSA(byte[] plaintext, String userID) {
		if (plaintext.length > m_intMaxCipherLength) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			try {
				byte[] part = __EncryptRSABlock(Arrays.copyOfRange(plaintext, 0, m_intMaxCipherLength), userID) ; 
				//System.out.println("[DPOS-ENCRYPTION] Ciphered " + new String(part, StandardCharsets.UTF_8));
				outputStream.write(part );
				outputStream.write( EncryptRSA(Arrays.copyOfRange(plaintext, m_intMaxCipherLength, plaintext.length),userID) );
			} catch (IOException e) {
				e.printStackTrace();
			}

			return outputStream.toByteArray();
		} else {
			return __EncryptRSABlock(plaintext,userID);
		}
	}

	/**
	 * Decrypt data with the RSA key pair of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param ciphertext is the data to be decrypted.
	 */
	public byte[] DecryptRSA(byte[] ciphertext, String userID) {
		String[] parts = new String(ciphertext,StandardCharsets.UTF_8).split("vault:v1:");
		String part;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
		for(int i=1; i< parts.length-1; i++)
		{
			part = parts[i];
			try {
				System.out.println("[DPOS-DECRYPTION] Deciphering " + part);
				outputStream.write( __DecryptRSABlock("vault:v1:"+part+"=",userID));
			} catch (IOException e) {			
				e.printStackTrace();
			}
		}
		part = parts[parts.length-1];
		try {
			System.out.println("[DPOS-DECRYPTION] Deciphering " + part);
			outputStream.write( __DecryptRSABlock("vault:v1:"+part,userID));
		} catch (IOException e) {			
			e.printStackTrace();
		}
		return outputStream.toByteArray();
	}

	/**
	 * Encrypt a block of data of size {@value m_intMaxCipherLength}
	 * with the RSA key pair of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param plaintext is the data to be encrypted.
	 */
	private byte[] __EncryptRSABlock(byte[] plaintext, String userID) {
		return m_temTransit
				.encrypt("RSA"+ userID, plaintext,VaultTransitContext.empty())
				.getBytes();
	}

	/**
	 * Decrypt a block of data of size {@value m_intMaxCipherLength}
	 * with the RSA key pair of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param ciphertext is the data to be decrypted.
	 */
	private byte[] __DecryptRSABlock(String ciphertext, String userID) {
		return m_temTransit
				.decrypt("RSA"+ userID,ciphertext)
				.getBytes();
	}



}