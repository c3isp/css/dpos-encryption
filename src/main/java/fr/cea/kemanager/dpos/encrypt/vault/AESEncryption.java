package fr.cea.kemanager.dpos.encrypt.vault;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.vault.core.VaultTransitTemplate;
import org.springframework.vault.support.VaultTransitContext;

import fr.cea.kemanager.dpos.encrypt.helper.Tools;
import sun.misc.BASE64Decoder;

/**
 * This class encapsulates the operations for AES encryption from Vault.
 *
 * @author Quentin Lutz
 */
public class AESEncryption {

	private static Logger log = LoggerFactory.getLogger(AESEncryption.class);

	private VaultTransitTemplate m_temTransit;

	/**
	 * Create a new {@link AESEncryption} with a {@link VaultTransitTemplate}.
	 *
	 * @param transit must not be {@literal null}.
	 */
	public AESEncryption(VaultTransitTemplate transit) {
		this.m_temTransit = transit;
	}


	/**
	 * Encrypt data with the AES key pair of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param plaintext is the data to be encrypted.
	 * @return string in base64
	 */
	public String EncryptAES(String plaintextBase64, String userID) {
		
		String content="";
		try {
			content = new String( 
					Base64.getDecoder().decode(plaintextBase64.getBytes()), 				 
					StandardCharsets.UTF_8);
		} catch (IllegalArgumentException e) {			
			if(e.toString().contains("Illegal base64 character"))
			{
				content = plaintextBase64; 
			}
			e.printStackTrace();
		}
		
		//log.debug("[DEBUG-BEFORE-DPOS-ENCRYPTION] HASH : " + Tools.encodeHmacSHA(content.getBytes()) );
		
		String encryptedMessage = m_temTransit.encrypt(
											"AES"+ userID, 
											content.getBytes(),
											VaultTransitContext.empty());
		String encryptedMessageBase64 = Base64.getEncoder().encodeToString(encryptedMessage.getBytes());
		//log.debug("[DEBUG-AFTER-DPOS-ENCRYPTION] HASH : " + Tools.encodeHmacSHA(encryptedMessageBase64.getBytes()) );
		return encryptedMessageBase64;
	}

	/**
	 * Decrypt data with the AES key pair of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param ciphertext is the data to be decrypted.
	 * @return string in base64
	 */
	public String DecryptAES(String ciphertext, String userID) {
		/*java.lang.IllegalArgumentException: Illegal base64 character 3a
		 * 
		 * */
		String content = "";
		String decryptedMessageBase64 = "ERROR";
		boolean isErrorDecode = false;
		/*DECODE Base 64 now */
		try {
		content = new String( 
				Base64.getDecoder().decode(ciphertext.getBytes()), 				 
				StandardCharsets.UTF_8);		
		} catch (IllegalArgumentException e) {
			log.debug("[DPOS-DECRYPTION]IllegalArgumentException :" + e.toString());
			/*Using */
//			log.debug("[DEBUG-DPOS-FAIL_DECODE64-DECRYPTION] <contentHash64>" + 
//						Tools.encodeHmacSHA(ciphertext.getBytes())+"</contentHash64>"); 			
			BASE64Decoder decoder = new BASE64Decoder();
			log.debug("[DPOS-DECRYPTION] trying use sun.BASE64Decoder now !");			
			try {
				content = new String(decoder.decodeBuffer(ciphertext), StandardCharsets.UTF_8);
				//log.debug("[DEBUG-DPOS-SUNDECODE64-DECRYPTION] HASH = " + Tools.encodeHmacSHA(content.getBytes())); 						
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				log.info("[DPOS-DECRYPTION] using directly without base64 !");
				content = ciphertext; 
				isErrorDecode = true;
			}
		}
		
		/*DECRYPT now */
		String decryptedMessage = "";
		if(isErrorDecode)
			log.info("[DPOS-DECRYPTION] Catched ERROR from Base64.Decode of decrypted message Done !");
		
		decryptedMessage = m_temTransit.decrypt("AES"+ userID,content);
		//log.debug("[DPOS-DECRYPTION] AFTER DECRYPTING : Hash Content file " + Tools.encodeHmacSHA(decryptedMessage.getBytes()));		
		
		decryptedMessageBase64 = Base64.getEncoder().encodeToString(decryptedMessage.getBytes());		
		//log.info("[DPOS-DECRYPTION] AFTER DECRYPTING and ENCODE 64 : Hash Content file " + Tools.encodeHmacSHA(decryptedMessageBase64.getBytes()));		

		return decryptedMessageBase64;
	}
}
