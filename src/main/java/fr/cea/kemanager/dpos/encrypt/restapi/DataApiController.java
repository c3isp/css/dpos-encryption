package fr.cea.kemanager.dpos.encrypt.restapi;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.kemanager.dpos.encrypt.helper.*;
import fr.cea.kemanager.lib.DataObject;
import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-16T10:31:18.377Z")

@RestController
@RequestMapping("/v1")
public class DataApiController implements DataApi {

	@Value("${token.vault.c3isp.cnr.it}")
	String TokenRoot;

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public DataApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void handle(Exception e) {
	    log.warn("[DPOS-ENCRYPTION/DECRYPTION] Returning HTTP 400 Bad Request", e);
	}

	@Override
	public 
	ResponseEntity<DataObject> decrypt(
			@ApiParam(value = "The CTI content to upload." ,required=true )  @Valid @RequestBody DataObject fileData,
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {				
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			if (getAcceptHeader().get().contains("application/json")) {
				try {
					
					log.info("[DPOS-DECRYPTION] Decryption request " + requestId + " DSAID = " + dsAId + " with size [" + fileData.toString().length() + "]");					
					if(fileData.getOtherProperties() != null && fileData.getOtherProperties().size() > 0)
					{						
						String fieldToDecrypt 	= fileData.getOtherProperties().get(Constants.DefaultParam.ENCRYPTED_FIELD_PARAM);
						String offsetValues		= fileData.getOtherProperties().get(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM);
						if(fieldToDecrypt != null && fieldToDecrypt.length() > 0)
						{
							return Tools.decryptDataWithKreyvium(fieldToDecrypt, offsetValues, dsAId, fileData.getDataContent(), TokenRoot);
						}						
					}					
					return Tools.decryptDataWithAES(fileData, requestId, dsAId, TokenRoot);
					
				} catch (IOException e) {
					log.error("Couldn't serialize response for content type application/json", e);
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		} else {
			log.warn("[DPOS-DECRYPTION] ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}


	/*
	 * DataObject fileData : it's the file to encrypt for creating bundle
	 * in the case where fileData contains OtherProperties (we used this fields for encrypting 
	 * a field text in fileData with Kreyvium security in order to make transciphering later
	 * */
	@Override
	public ResponseEntity<DataObject> encrypt(
			@ApiParam(value = "The CTI content to upload." ,required=true )  @Valid @RequestBody DataObject fileData,
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {				
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			if (getAcceptHeader().get().contains("application/json")) {
				try {											
					if(fileData.getOtherProperties() != null && fileData.getOtherProperties().size() > 0)
					{						
						String fieldToEncrypt = fileData.getOtherProperties().get(Constants.DefaultParam.ENCRYPTED_FIELD_PARAM);						
						if (fieldToEncrypt != null && fieldToEncrypt.length() > 0 ) {
							log.info("[DPOS-ENCRYPTION] Received Transciphering request " + requestId + " DSAID = " + dsAId 
									+ " with length object = " + fileData.getDataContent().length());	
							return Tools.applyingTranscipheringProcess(fileData, dsAId, fieldToEncrypt, TokenRoot);
						}
					}
					
					log.info("[DPOS-ENCRYPTION] Received Encryption request " + requestId + " DSAID = " + dsAId 
							+ " with length object = " + fileData.getDataContent().length());					
					return Tools.applyingEncryptionBundle(fileData, requestId, dsAId, TokenRoot);
					
				} catch (IOException e) {
					log.error("Couldn't serialize response for content type application/json", e);
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		} else {
			log.warn("[DPOS-ENCRYPTION] ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
		}
		log.warn("[DPOS-ENCRYPTION] ERROR encryption request !");
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}
}
